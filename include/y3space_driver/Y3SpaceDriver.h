#ifndef _Y3SPACE_DRIVER_H
#define _Y3SPACE_DRIVER_H

#include <ros/ros.h>
#include <sensor_msgs/Imu.h>
#include <imu_data/Imu_raw.h>
#include <std_msgs/Float64.h>
#include <SerialInterface.h>


//! \brief Yost Labs 3-Space ROS Driver Class
class Y3SpaceDriver: SerialInterface
{
public:
    //!
    //! Constructor
    //!
    Y3SpaceDriver(ros::NodeHandle& nh, ros::NodeHandle& pnh, std::string port, int baudrate, int timeout, std::string mode, std::string frame, std::string settingsFile);
    //!
    //! Destructor
    //!
    ~Y3SpaceDriver();
    //!
    //! \brief run: runs system
    //!
    void run(void);
    //!
    //! \brief getSoftwareVersion
    //! \return returns software string version
    //!
    const std::string getSoftwareVersion(void);
    //!
    //! \brief getHardwareVersion
    //! \return returns hardware string version
    //!
    const std::string getHardwareVersion(void);
    //!
    //! \brief restoreFactorySettings resets everything
    //!
    void restoreFactorySettings(void);
    //!
    //! \brief getAxisDirection
    //! \return returns axis directions
    //!
    const std::string getAxisDirection(void);
    //!
    //! \brief getCalibMode
    //! \return 0 for bias 1 for scale and bias
    //!
    const std::string getCalibMode(void);
    //!
    //! \brief getMIMode
    //! \return 1 if enabled 0 if disabled
    //!
    const std::string getMIMode(void);
    //!
    //! \brief startGyroCalibration
    //!
    void startGyroCalibration(void);
    //!
    //! \brief setMIMode
    //! \param on True to set , False to Unset
    //!
    void setMIMode(bool on);
	//!
	//! \brief setCalibrationParameters
	//	\param filepath where the XML formatted file containing settings is
	//	stored
	void setCalibrationParameters(std::string filepath);

private:
    // ROS Member Variables
    ros::NodeHandle m_nh;     ///< Nodehandle for the driver node
    ros::NodeHandle m_pnh;    ///< Private Nodehandle for use with Serial Interface
    ros::Publisher m_imuPub;  ///< Publisher for IMU messages
    // ros::Publisher m_tempPub; ///< Publisher for temperature messages
    std::string m_mode;       ///< String indicating the desired driver mode
    std::string m_frame;      ///< The frame ID to broadcast to tf
    std::string m_settingsFile; ///< The file with settings to calibrate the device
    static const std::string logger; ///< Logger tag

    static const std::string MODE_ABSOLUTE;
    static const std::string MODE_RELATIVE;

    /*
     * Below is a list of commands that can be written via the
     * serialWrite() function to send raw commands to the 3-Space
     * firmware. Unless otherwise noted, these are derived directly
     * from the 3-Space API
     */

    // Orientation Sensor Data Commands
    static constexpr auto GET_TARED_ORIENTATION_AS_QUATERNION        = ":0\n";
    static constexpr auto GET_TARED_ORIENTATION_AS_EULER_ANGLES      = ":1\n";
    static constexpr auto GET_TARED_ORIENTATION_AS_ROTATION_MATRIX   = ":2\n";
    static constexpr auto GET_TARED_ORIENTATION_AS_AXIS_ANGLE        = ":3\n";
    static constexpr auto GET_TARED_ORIENTATION_AS_TWO_VECTOR        = ":4\n";
    static constexpr auto GET_DIFFERENCE_QUATERNION                  = ":5\n";
    static constexpr auto GET_UNTARED_ORIENTATION_AS_QUATERNION      = ":6\n";
    static constexpr auto GET_UNTARED_ORIENTATION_AS_EULER_ANGLES    = ":7\n";
    static constexpr auto GET_UNTARED_ORIENTATION_AS_ROTATION_MATRIX = ":8\n";
    static constexpr auto GET_UNTARED_ORIENTATION_AS_AXIS_ANGLE      = ":9\n";
    static constexpr auto GET_UNTARED_ORIENTATION_AS_TWO_VECTOR      = ":10\n";
    static constexpr auto GET_TARED_TWO_VECTOR_IN_SENSOR_FRAME       = ":11\n";
    static constexpr auto GET_UNTARED_TWO_VECTOR_IN_SENSOR_FRAME     = ":12\n";

    // Corrected Raw Data Commands
    static constexpr auto GET_ALL_CORRECTED_COMPONENT_SENSOR                = ":37\n";
    static constexpr auto GET_CORRECTED_GYRO_RATE                           = ":38\n";
    static constexpr auto GET_CORRECTED_ACCELEROMETER_VECTOR                = ":39\n";
    static constexpr auto GET_CORRECTED_COMPASS_VECTOR                      = ":40\n";
    static constexpr auto GET_CORRECTED_LINEAR_ACCELERATION_IN_GLOBAL_SPACE = ":41\n";
    static constexpr auto CORRECT_RAW_GYRO_DATA                             = ":48\n";
    static constexpr auto CORRECT_RAW_ACCEL_DATA                            = ":49\n";
    static constexpr auto CORRECT_RAW_COMPASS_DATA                          = ":50\n";

    // Misc. Raw Data Commands
    static constexpr auto GET_TEMPERATURE_C     = ":43\n";
    static constexpr auto GET_TEMPERATURE_F     = ":44\n";
    static constexpr auto GET_CONFIDENCE_FACTOR = ":45\n";

    // Uncorrected Raw Data Commands
    static constexpr auto GET_ALL_RAW_COMPONENT_SENSOR_DATA = ":64\n";
    static constexpr auto GET_RAW_GYRO_RATE                 = ":65\n";
    static constexpr auto GET_RAW_ACCEL_DATA                = ":66\n";
    static constexpr auto GET_RAW_COMPASS_DATA              = ":67\n";

    // Streaming Commands
    static constexpr auto SET_STREAMING_SLOTS_EULER_TEMP        = ":80,1,43,255,255,255,255,255,255\n";
    static constexpr auto SET_STREAMING_SLOTS_EULER_QUATERNION  = ":80,1,0,255,255,255,255,255,255\n";
    static constexpr auto SET_STREAMING_SLOTS_QUATERNION_EULER  = ":80,0,1,255,255,255,255,255,255\n";
    static constexpr auto SET_STREAMING_SLOTS_EULER             = ":80,1,255,255,255,255,255,255,255\n";
    static constexpr auto SET_STREAMING_SLOTS_QUATERNION        = ":80,0,255,255,255,255,255,255,255\n";
    static constexpr auto SET_STREAMING_SLOTS_QUATERNION_CORRECTED_GYRO_ACCELERATION_LINEAR_IN_GLOBAL = ":80,0,38,41,255,255,255,255,255\n";

    // ROS IMU Configurations -----------------------------------------------------------------------------
    /*
    * Most of the above commands are standard command expressions
    * from the API, but below are modified versions for use with
    * this ROS driver
    *
    * TODO: Figure out if you can get the covariance
    */
    static constexpr auto SET_STREAMING_SLOTS_ROS_IMU_RELATIVE  = ":80,0,38,41,43,255,255,255,255\n";
    static constexpr auto SET_STREAMING_SLOTS_ROS_IMU_ABSOLUTE  = ":80,6,38,41,43,255,255,255,255\n";
    static constexpr auto SET_STREAMING_SLOTS_ROS_IMU_YOSTC  = ":80,41,38,40,0,255,255,255,255\n";
    static constexpr auto SET_STREAMING_SLOTS_ROS_IMU_YOSTC2 = ":80,39,38,40,6,255,255,255,255\n";
    static constexpr auto SET_STREAMING_SLOTS_ROS_IMU_YOSTN  = ":80,34,33,35,0,255,255,255,255\n";
    static constexpr auto SET_STREAMING_SLOTS_ROS_IMU_YOSTR  = ":80,66,65,67,0,255,255,255,255\n";
    static constexpr auto SET_STREAMING_SLOTS_ROS_IMU_YOSTR2  = ":80,66,65,67,6,255,255,255,255\n";
    static constexpr auto SET_STREAMING_SLOTS_ROS_IMU_YOSTR3  = ":80,64,0,255,255,255,255,255,255\n";
    // ----------------------------------------------------------------------------------------------------

    static constexpr auto GET_STREAMING_SLOTS                   = ":81\n";
    static constexpr auto SET_STREAMING_TIMING_0_MS             = ":82,0,0,4294967296\n";
    static constexpr auto SET_STREAMING_TIMING_1_MS             = ":82,1000,0,0\n";
    static constexpr auto SET_STREAMING_TIMING_10_MS            = ":82,10000,0,0\n";
    static constexpr auto SET_STREAMING_TIMING_14_MS            = ":82,14000,0,0\n";
    static constexpr auto SET_STREAMING_TIMING_15_MS            = ":82,15000,0,0\n";
    static constexpr auto SET_STREAMING_TIMING_20_MS            = ":82,20000,0,0\n";
    static constexpr auto SET_STREAMING_TIMING_28_MS            = ":82,28000,0,0\n";
    static constexpr auto SET_STREAMING_TIMING_30_MS            = ":82,30000,0,0\n";
    static constexpr auto SET_STREAMING_TIMING_50_MS            = ":82,50000,0,0\n";
    static constexpr auto SET_STREAMING_TIMING_100_MS           = ":82,100000,0,0\n";
    static constexpr auto SET_STREAMING_TIMING_1000_MS          = ":82,1000000,0,0\n";
    static constexpr auto SET_STREAMING_TIMING_5000_MS          = ":82,5000000,0,0\n";
    static constexpr auto GET_STREAMING_TIMING                  = ":83\n";
    static constexpr auto GET_STREAMING_BATCH                   = ":84\n";
    static constexpr auto START_STREAMING                       = ":85\n";
    static constexpr auto STOP_STREAMING                        = ":86\n";
    static constexpr auto UPDATE_CURRENT_TIMESTAMP              = ":95\n";

    // Settings Configuration READ Commands
    static constexpr auto GET_AXIS_DIRECTION           = ":143\n";
    static constexpr auto GET_FILTER_MODE              = ":152\n";
    static constexpr auto GET_EULER_DECOMPOSTION_ORDER = ":156\n";
    static constexpr auto GET_MI_MODE_ENABLED          = ":136\n";

    // Settings Configuration WRITE Commands
    static constexpr auto SET_EULER_ANGLE_DECOMP_ORDER_XYZ = ":16,0\n";
    static constexpr auto SET_EULER_ANGLE_DECOMP_ORDER_YZX = ":16,1\n";
    static constexpr auto SET_EULER_ANGLE_DECOMP_ORDER_ZXY = ":16,2\n";
    static constexpr auto SET_EULER_ANGLE_DECOMP_ORDER_ZYX = ":16,3\n";
    static constexpr auto SET_EULER_ANGLE_DECOMP_ORDER_XZY = ":16,4\n";
    static constexpr auto SET_EULER_ANGLE_DECOMP_ORDER_YXZ = ":16,5\n";
    static constexpr auto OFFSET_WITH_CURRENT_ORIENTATION  = ":19\n";
    static constexpr auto TARE_WITH_CURRENT_ORIENTATION    = ":96\n";
    static constexpr auto TARE_WITH_CURRENT_QUATERNION     = ":97\n";
    static constexpr auto SET_MI_MODE_ENABLED              = ":112,1\n";
    static constexpr auto SET_MI_MODE_DISABLED             = ":112,0\n";
    static constexpr auto BEGIN_MI_MODE_FIELD_CALIBRATION  = ":114\n";
    static constexpr auto SET_AXIS_DIRECTIONS_ENU          = ":116,8\n";
    static constexpr auto SET_AXIS_DIRECTIONS_DEFAULT      = ":116,5\n";

    // Calibration Commands
    static constexpr auto BEGIN_GYRO_AUTO_CALIB       = ":165\n";
    static constexpr auto SET_CALIB_MODE_BIAS         = ":169,0\n";
    static constexpr auto SET_CALIB_MODE_SCALE_BIAS   = ":169,1\n";
    static constexpr auto GET_CALIB_MODE              = ":170\n";

    // System Commands
    static constexpr auto GET_FIRMWARE_VERSION_STRING = ":223\n";
    static constexpr auto GET_HARDWARE_VERSION_STRING = ":230\n";
    static constexpr auto RESTORE_FACTORY_SETTINGS    = ":224\n";
    static constexpr auto SOFTWARE_RESET              = ":226\n";

    // Hardware Types
    static constexpr auto Y3SPACE_BLUETOOTH           = "TSS-BT-H3               v2.0.0  \r\n";
    static constexpr auto Y3SPACE_MINI_BLUETOOTH      = "TSS-MBT                 v2.0.0  \r\n";
};
#endif //_Y3SPACE_DRIVER_H
