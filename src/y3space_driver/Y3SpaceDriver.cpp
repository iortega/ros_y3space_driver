#include <Y3SpaceDriver.h>
#include <time.h>
#include <chrono>
#include <thread>
#include <iostream>
#include <fstream>
#include <pugixml.hpp>
using namespace std;

const std::string Y3SpaceDriver::logger = "[ Y3SpaceDriver ] ";
const std::string Y3SpaceDriver::MODE_ABSOLUTE = "absolute";
const std::string Y3SpaceDriver::MODE_RELATIVE = "relative";

Y3SpaceDriver::Y3SpaceDriver(ros::NodeHandle& nh,
                             ros::NodeHandle& pnh,
                             std::string port,
                             int baudrate,
                             int timeout,
                             std::string mode,
                             std::string frame,
                             std::string settingsFile
							 ):
    SerialInterface(port, baudrate, timeout),
    m_pnh(pnh),
    m_nh(nh),
    m_mode(mode),
    m_frame(frame),
	m_settingsFile(settingsFile)
{
    this->serialConnect();
    this->m_imuPub = this->m_nh.advertise<imu_data::Imu_raw>("/imu", 10);
    // this->m_tempPub = this->m_nh.advertise<std_msgs::Float64>("/imu/temp", 10);
}

Y3SpaceDriver::~Y3SpaceDriver() {}

void Y3SpaceDriver::restoreFactorySettings()
{
    this->serialWriteString(RESTORE_FACTORY_SETTINGS);
}

const std::string Y3SpaceDriver::getSoftwareVersion()
{
    this->serialWriteString(GET_FIRMWARE_VERSION_STRING);

    const std::string buf = this->serialReadLine();
    ROS_INFO_STREAM(this->logger << "Software version: " << buf);
    return buf;
}

const std::string Y3SpaceDriver::getHardwareVersion()
{
    this->serialWriteString(GET_HARDWARE_VERSION_STRING);

    const std::string buf = this->serialReadLine();
    ROS_INFO_STREAM(this->logger << "Hardware version: " << buf);
    return buf;
}

const std::string Y3SpaceDriver::getAxisDirection()
{
    this->serialWriteString(GET_AXIS_DIRECTION);

    const std::string buf = this->serialReadLine();
    const std::string ret = [&]()
    {
        if(buf == "0\r\n")
        {
            return "X: Right, Y: Up, Z: Forward";
        }
        else if ( buf == "1\r\n")
        {
            return "X: Right, Y: Forward, Z: Up";
        }
        else if ( buf == "2\r\n")
        {
            return "X: Up, Y: Right, Z: Forward";
        }
        else if (buf == "3\r\n")
        {
            return "X: Forward, Y: Right, Z: Up";
        }
        else if( buf == "4\r\n")
        {
            return "X: Up, Y: Forward, Z: Right";
        }
        else if( buf == "5\r\n")
        {
            return "X: Forward, Y: Up, Z: Right";
        }
        else if (buf == "19\r\n")
        {
            return "X: Forward, Y: Left, Z: Up";
        }
        else
        {
            ROS_WARN_STREAM(this->logger << "Buffer indicates: " + buf);
            return "Unknown";
        }
    }();

    ROS_INFO_STREAM(this->logger << "Axis Direction: " << ret);
    return ret;
}

void Y3SpaceDriver::startGyroCalibration(void)
{
    ROS_INFO_STREAM(this->logger << "Starting Auto Gyro Calibration...");
    this->serialWriteString(BEGIN_GYRO_AUTO_CALIB);

    ros::Duration(5.0).sleep();
    ROS_INFO_STREAM(this->logger << "Proceeding");
}

void Y3SpaceDriver::setMIMode(bool on)
{
    if(on)
    {
        this->serialWriteString(SET_MI_MODE_ENABLED);
    }
    else
    {
        this->serialWriteString(SET_MI_MODE_DISABLED);
    }
}

void Y3SpaceDriver::setCalibrationParameters(std::string filepath)
{
	if (!filepath.empty())
	{
		pugi::xml_document doc;
		std::vector<std::string> gyro, accel, compass;
		pugi::xml_parse_result result = doc.load_file(filepath.c_str());
		std::string SET_GYRO_CALIBRATION_COEFFICIENTS = ":166";
		std::string SET_ACCEL_CALIBRATION_COEFFICIENTS = ":161";
		std::string SET_COMPASS_CALIBRATION_COEFFICIENTS = ":160";
		ROS_INFO_STREAM(this->logger << "Calibrating device...");
		if (!result)
			ROS_INFO_STREAM(this->logger << "Configuration file not found, no calibration will be made!!");
		else {
			for (pugi::xml_node index: doc.first_child().child("Gyro").children()) {
				SET_GYRO_CALIBRATION_COEFFICIENTS.append(",");
				SET_GYRO_CALIBRATION_COEFFICIENTS.append(index.child_value());
			}

			for (pugi::xml_node index: doc.first_child().child("Accel").children()) {
				SET_ACCEL_CALIBRATION_COEFFICIENTS.append(",");
				SET_ACCEL_CALIBRATION_COEFFICIENTS.append(index.child_value());
			}

			for (pugi::xml_node index: doc.first_child().child("Compass").children()) {
				SET_COMPASS_CALIBRATION_COEFFICIENTS.append(",");
				SET_COMPASS_CALIBRATION_COEFFICIENTS.append(index.child_value());
			}

			SET_COMPASS_CALIBRATION_COEFFICIENTS.append("\n");
			SET_ACCEL_CALIBRATION_COEFFICIENTS.append("\n");
			SET_GYRO_CALIBRATION_COEFFICIENTS.append("\n");

			this->serialWriteString(SET_COMPASS_CALIBRATION_COEFFICIENTS);
			this->serialWriteString(SET_ACCEL_CALIBRATION_COEFFICIENTS);
			this->serialWriteString(SET_GYRO_CALIBRATION_COEFFICIENTS);
		}
	}
}

const std::string Y3SpaceDriver::getCalibMode()
{
	this->serialWriteString(GET_CALIB_MODE);

	const std::string buf = this->serialReadLine();
	const std::string ret = [&]()
	{
		if(buf == "0\r\n")
		{
			return "Bias";
		}
		else if ( buf == "1\r\n")
		{
			return "Scale and Bias";
		}
		else
		{
			ROS_WARN_STREAM(this->logger << "Buffer indicates: " + buf);
			return "Unknown";
		}
	}();

	ROS_INFO_STREAM(this->logger << "Calibration Mode: " << ret);
	return ret;
}

const std::string Y3SpaceDriver::getMIMode()
{
	this->serialWriteString(GET_MI_MODE_ENABLED);

	const std::string buf = this->serialReadLine();
	const std::string ret = [&]()
	{
		if(buf == "0\r\n")
		{
			return "Disabled";
		}
		else if ( buf == "1\r\n")
		{
			return "Enabled";
		}
		else
		{
			ROS_WARN_STREAM(this->logger << "Buffer indicates: " + buf);
			return "Unknown";
		}
	}();

	ROS_INFO_STREAM(this->logger << "MI Mode: " << ret);
	return ret;
}

//! Run the serial sync
void Y3SpaceDriver::run()
{
	std::vector<double> parsedVals;
	imu_data::Imu_raw imuMsg;
	// std_msgs::Float64 tempMsg;
	std::string swVersion;
	std::string hwVersion;
	bool print2file = false;
	ofstream myfile;

	/*	ofstream myfile("/home/scpjaize/example.txt", ios::out | ios::binary);
		myfile << "Writing this to a file.\n";
		myfile.close();
		*/
	if (print2file){
		myfile.open("/home/initega/catkin_ws/yost.txt", ios::out | ios::binary);
		myfile << "// Start Time: 0" << endl;
		myfile << "// Sample rate: 10.0Hz" << endl;
		myfile << "// Scenario: 4.9" << endl;
		myfile << "// Firmware Version: 2.5.1" << endl;
		myfile << "Counter	Acc_X	Acc_Y	Acc_Z	Gyr_X	Gyr_Y	Gyr_Z	Mag_X	Mag_Y	Mag_Z	Quat_w	Quat_x	Quat_y	Quat_z"<< endl;
	}
	int kont = 0;
	int fs;

	swVersion = this->getSoftwareVersion();
	hwVersion = this->getHardwareVersion();
	this->getAxisDirection();
	this->getMIMode();
	/*if (m_mode == MODE_ABSOLUTE)
	  {
	  ROS_INFO_STREAM(this->logger << "Using absolute driver stream configuration");
	  this->serialWriteString(SET_STREAMING_SLOTS_ROS_IMU_ABSOLUTE);
	  }
	  else if (m_mode == MODE_RELATIVE)
	  {
	  ROS_INFO_STREAM(this->logger << "Using relative driver stream configuration");
	  this->serialWriteString(SET_STREAMING_SLOTS_ROS_IMU_RELATIVE);
	  }
	  else
	  {
	  ROS_WARN_STREAM(this->logger << "Unknown driver mode set... Defaulting to relative");
	  this->serialWriteString(SET_STREAMING_SLOTS_ROS_IMU_RELATIVE);
	  }*/
	/* this->setCalibrationParameters(m_settingsFile); */
	/* this->startGyroCalibration(); */
	this->serialWriteString(SET_STREAMING_SLOTS_ROS_IMU_YOSTR);
	this->serialWriteString(TARE_WITH_CURRENT_ORIENTATION);
	this->serialWriteString(TARE_WITH_CURRENT_QUATERNION);

	if (hwVersion == Y3SPACE_BLUETOOTH) {
		this->getCalibMode();
		this->serialWriteString(SET_STREAMING_TIMING_30_MS);//0
		fs = 33;
	}
	else if (hwVersion == Y3SPACE_MINI_BLUETOOTH) {
		this->serialWriteString(SET_STREAMING_TIMING_15_MS);
		fs = 66;
	}
	else {
		this->getCalibMode();
		this->serialWriteString(SET_STREAMING_TIMING_100_MS);
		fs = 10;
	}

	this->serialWriteString(START_STREAMING);
	ROS_INFO_STREAM(this->logger << "Ready\n");

	ros::Rate rate(66);
	int line = 0;
	while(ros::ok())
	{
		// ROS_INFO_STREAM(this->logger << "0");
		while(this->available() > 0)
		{
			// ROS_INFO_STREAM(this->logger << "1");
			line += 1;
			std::string buf = this->serialReadLine();
			// ROS_INFO_STREAM(this->logger << line << " : " << buf);
			std::string parse;
			std::stringstream ss(buf);
			double i;
			// ROS_INFO_STREAM(this->logger << "2");

			// Parse data from the line
			while (ss >> i)
			{
				parsedVals.push_back(i);
				if (ss.peek() == ',')
					ss.ignore();
			}

			// ROS_INFO_STREAM(this->logger << "3");
			// Should stop reading when line == number of tracked streams
			if(line == 4)
			{
				// Reset line tracker
				line = 0;

				// Prepare IMU message
				imuMsg.header.stamp           = ros::Time::now();
				imuMsg.header.frame_id        = m_frame;
				imuMsg.acceleration.x          = parsedVals[0];
				imuMsg.acceleration.y          = parsedVals[1];
				imuMsg.acceleration.z          = parsedVals[2];
				imuMsg.gyro.x     = parsedVals[3];
				imuMsg.gyro.y     = parsedVals[4];
				imuMsg.gyro.z     = parsedVals[5];
				imuMsg.magnetometer.x  = parsedVals[6];
				imuMsg.magnetometer.y  = parsedVals[7];
				imuMsg.magnetometer.z  = parsedVals[8];

				/* std::cout << kont << "\n"; */

				// Prepare temperature message
				// tempMsg.data = parsedVals[10];
				if (print2file) {
					myfile <<" ";
					myfile << kont << "\t";
					for (int ind=0;ind<13;ind++)
					{
						//std::cout << ind<<":   "<< parsedVals[ind]<<std::endl;
						myfile << parsedVals[ind] << "\t";
					}
					// Clear parsed values
					myfile << "\n";
				}
				parsedVals.clear();

				this->m_imuPub.publish(imuMsg);
				// this->m_tempPub.publish(tempMsg);
				// ROS_INFO_STREAM(this->logger << "3.5");
				kont=kont+1;
				switch (kont) {
					case 300:
						std::cout << "300" << std::endl;
						break;
					case 1:
						std::cout << "START" << std::endl;
						break;
				}
			}
		}

		// ROS_INFO_STREAM(this->logger << "4");
		// Throttle ROS at fixed Rate
		rate.sleep();
		// ROS_INFO_STREAM(this->logger << "5");
		ros::spinOnce();
		// ROS_INFO_STREAM(this->logger << "6");
	}
	if (print2file)
		myfile.close();
	std::cout << kont/(float) fs <<std::endl;
	this->serialWriteString(STOP_STREAMING);
}
